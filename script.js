const form = document.getElementById("form");

const email = form.elements["email"];
console.log();

form.addEventListener("submit", (event) => {
  event.preventDefault();

  var count = 0;
  var validatePass = false;
  var validateRole = false;
  var validatePermission = false;

  //No need for email check HTML buil in validations works fine for this assignment

  //Check for password , regex source: https://www.w3resource.com/javascript/form/password-validation.php
  var pass = event.target[1].value;
  var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,12}$/;

  if (pass.match(passw)) {
    validatePass = true;
  } else {
    alert(
      "Passsword should be minimum of 6 character with MIX of Uppercase, lowercase, digits"
    );
  }

  //accessing elements of form
  var elem = document.getElementById("form").elements;

  //Check for role
  if (event.target[3].checked || event.target[4].checked) {
    var roleSelected = event.target[3].checked
      ? event.target[3].value
      : event.target[4].value;
    validateRole = true;
  } else {
    alert("Choose a role please either Admin or User");
  }

  //Permission check
  var perms = [];
  for (var i = 5; i < elem.length - 1; i++) {
    if (event.target[i].checked) {
      count += 1;
      perms.push(event.target[i].value);
    }
  }

  if (count < 2) {
    alert("Altlest 2 permission are required!");
  } else {
    validatePermission = true;
  }

  //After submmiting the form
  if (validatePass && validatePermission && validateRole) {
    document.getElementById(
      "form"
    ).innerHTML = ` <h1>Email: ${event.target[0].value}</h1>
                                                        <h1>Password: ${event.target[1].value}<h1>
                                                        <h1>Sex: ${event.target[2].value}</h1>
                                                        <h1>Role: ${roleSelected}</h1>
                                                        <h1>Permissions: ${perms}</h1>`;

    document.getElementById("confirm").removeAttribute("hidden");
  }
});
